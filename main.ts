import { array, id, int, object, string } from 'cast.ts'
import playwright, { Page } from 'playwright'
import { collectTableWithHeaders } from 'playwright-table'

let searchByNameParser = object({
  more: int(), // 1 | 0
  stockInfo: array(
    object({
      stockId: id(),
      code: string(),
      name: string(),
    }),
  ),
})

async function searchByName(options: {
  name: string // e.g. 02007
  market?: string // e.g. SEHK
  type?: 'A' | 'I'
  // A (Active) -> Current Securities
  // I (Inactive) -> Delisted Securities
  all?: boolean
}) {
  let params = new URLSearchParams('?callback=callback&lang=EN')
  params.set('name', options.name)
  params.set('market', options.market || 'SEHK')
  params.set('type', options.type || 'A')
  let file = options.all ? 'partial.do' : 'prefix.do'
  let url = `https://www1.hkexnews.hk/search/${file}?${params}`
  let res = await fetch(url)
  let text = await res.text()
  let match = text.trim().match(/^callback\((.*)\);$/)
  if (!match) {
    console.error('invalid jsonp response:', text)
    throw new Error('invalid jsonp response')
  }
  let json = JSON.parse(match[1])
  try {
    return searchByNameParser.parse(json).stockInfo
  } catch (error) {
    console.error('invalid json payload:', json)
    throw error
  }
}

async function searchDetail(options: {
  page: Page
  stockId: number | string
  from: string
  to: string
}) {
  let { page } = options
  let url = `https://www1.hkexnews.hk/search/titlesearch.xhtml?lang=en`
  let params = new URLSearchParams(
    '?lang=EN&category=0&market=SEHK&searchType=0&documentType=-1&t1code=-2&t2Gcode=-2&t2code=-2&stockId=15008&from=19990401&to=20230807&MB-Daterange=1&title=',
  )
  params.set('stockId', String(options.stockId))
  params.set('from', options.from)
  params.set('to', options.to)
  let res = await fetch(url, {
    method: 'POST',
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    body: params.toString(),
  })
  let html = await res.text()
  await page.evaluate(html => {
    let parser = new DOMParser()
    let doc = parser.parseFromString(html, 'text/html')
    let table = doc.querySelector('table')
    if (!table) {
      throw new Error('Failed to find table in html response')
    }
    table
      .querySelectorAll('.mobile-list-heading')
      .forEach(span => span.remove())

    let th = document.createElement('th')
    th.textContent = 'Document Link'
    table.querySelector('thead tr')?.appendChild(th)

    table.querySelectorAll('tbody tr').forEach(tr => {
      let link = tr.querySelector<HTMLAnchorElement>('td .doc-link a')?.href
      let td = document.createElement('td')
      td.textContent = link || 'no-link'
      tr.appendChild(td)
    })

    document.body.innerHTML = table.outerHTML
  }, html)
  let table = await collectTableWithHeaders({ page, selector: 'table' })
  console.log(table)
}

async function main() {
  let browser = await playwright.chromium.launch({ headless: false })
  let page = await browser.newPage()

  await page.goto(`https://www1.hkexnews.hk/search/titlesearch.xhtml?lang=en`)

  // let infos = await searchByName({
  //   name: '02007',
  //   all: true,
  // })
  // console.log(infos)
  let detail = await searchDetail({
    page,
    stockId: 15008,
    from: '20080101',
    to: '20230301',
  })
  console.log(detail)

  // await page.close()
  // await browser.close()
}
main().catch(e => console.error(e))
