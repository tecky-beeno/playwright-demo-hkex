var pages = document.querySelectorAll('a[name]')

var pageList = []

for (let page of pages) {
  let pageName = page.name
  let pageContainer = document.createElement('div')
  pageContainer.className = 'page'
  let children = []
  children.push(page)
  let node = page.nextSibling
  while (node) {
    if (node.tagName?.toLowerCase() == 'a' && node.name) {
      break
    }
    children.push(node)
    node = node.nextSibling
  }
  pageList.push({
    pageName,
    pageContainer,
    children,
  })
}

var pageListContainer = document.createElement('div')
pageListContainer.className = 'page-list'

for (let page of pageList) {
  for (let node of page.children) {
    page.pageContainer.appendChild(node)
  }
  pageListContainer.appendChild(page.pageContainer)
}
document.body.appendChild(pageListContainer)

var outlinePage = pageList.find(page => page.pageName == 'outline')

function findAuditorReportPageNumbers() {
  var pageLinks = outlinePage.pageContainer.querySelectorAll('a')
  let startPageNumber
  let endPageNumber
  for (let pageLink of pageLinks) {
    let pageNumber = pageLink.href.split('#').pop()
    if (pageLink.innerText.toUpperCase() == 'INDEPENDENT AUDITOR’S REPORT') {
      startPageNumber = +pageNumber
      continue
    }
    if (startPageNumber) {
      endPageNumber = +pageNumber
      return { startPageNumber, endPageNumber }
    }
  }
}

var auditorReportPageNumbers = findAuditorReportPageNumbers()

var auditorReportText = ''

for (
  let pageNumber = auditorReportPageNumbers.startPageNumber;
  pageNumber < auditorReportPageNumbers.endPageNumber;
  pageNumber++
) {
  let page = pageList.find(page => page.pageName == pageNumber)
  auditorReportText += page.pageContainer.innerText + '\n'
}
