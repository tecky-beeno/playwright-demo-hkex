import { Poppler } from 'node-poppler'
import {
  accessSync,
  copyFileSync,
  mkdirSync,
  readdirSync,
  readFileSync,
  rmdirSync,
  unlinkSync,
  writeFileSync,
} from 'fs'
import { chromium, Page } from 'playwright'
import { randomUUID } from 'crypto'
import { basename, join } from 'path'

async function pdfToHTML(pdfFile: string) {
  let htmlFile = pdfFile.replace(/\.pdf$/, 's.html')

  try {
    accessSync(htmlFile)
  } catch (error) {
    // file not exist
    let poppler = new Poppler('/usr/bin')
    await poppler.pdfToHtml(pdfFile)
  }

  let html = readFileSync(htmlFile).toString()

  return html
}

async function extractAuditorReport(options: { page: Page; html: string }) {
  let { page } = options

  let auditorReport = await page.evaluate(html => {
    let parser = new DOMParser()
    let document = parser.parseFromString(html, 'text/html')

    var pages = document.querySelectorAll<HTMLAnchorElement>('a[name]')

    var pageList = []

    for (let page of pages) {
      let pageName = page.name
      let pageContainer = document.createElement('div')
      pageContainer.className = 'page'
      let children = []
      children.push(page)
      let node = page.nextSibling
      while (node) {
        if (
          node instanceof HTMLAnchorElement &&
          // node.tagName.toLowerCase() == 'a' &&
          node.name
        ) {
          break
        }
        children.push(node)
        node = node.nextSibling
      }
      pageList.push({
        pageName,
        pageContainer,
        children,
      })
    }

    var pageListContainer = document.createElement('div')
    pageListContainer.className = 'page-list'

    for (let page of pageList) {
      for (let node of page.children) {
        page.pageContainer.appendChild(node)
      }
      pageListContainer.appendChild(page.pageContainer)
    }
    document.body.appendChild(pageListContainer)

    var outlinePage = pageList.find(page => page.pageName == 'outline')

    function findAuditorReportPageNumbers() {
      if (!outlinePage) {
        throw new Error('Outline page not found')
      }

      var pageLinks = outlinePage.pageContainer.querySelectorAll('a')
      let startPageNumber
      let endPageNumber
      for (let pageLink of pageLinks) {
        let pageNumber = pageLink.href.split('#').pop()
        if (!pageNumber) continue
        if (
          pageLink.innerText.toUpperCase() == 'INDEPENDENT AUDITOR’S REPORT'
        ) {
          startPageNumber = +pageNumber
          continue
        }
        if (startPageNumber) {
          endPageNumber = +pageNumber
          return { startPageNumber, endPageNumber }
        }
      }

      throw new Error('independent auditor report not found')
    }

    var auditorReportPageNumbers = findAuditorReportPageNumbers()

    var auditorReportPages: string[] = []

    for (
      let pageNumber = auditorReportPageNumbers.startPageNumber;
      pageNumber < auditorReportPageNumbers.endPageNumber;
      pageNumber++
    ) {
      let page = pageList.find(page => +page.pageName == pageNumber)
      if (!page) {
        throw new Error('failed to find page ' + page)
      }
      auditorReportPages.push(page.pageContainer.innerText)
    }

    return {
      startPageNumber: auditorReportPageNumbers.startPageNumber,
      endPageNumber: auditorReportPageNumbers.endPageNumber,
      auditorReportPages,
    }
  }, options.html)

  return auditorReport
}

function rmdirRecursive(dir: string) {
  let filenames = readdirSync(dir)
  for (let filename of filenames) {
    let file = join(dir, filename)
    unlinkSync(file)
  }
  rmdirSync(dir)
}

export async function processPDF(options: { page: Page; pdfFile: string }) {
  let dir = '/tmp/' + randomUUID()
  mkdirSync(dir, { recursive: true })

  let tmpFile = join(dir, basename(options.pdfFile))
  copyFileSync(options.pdfFile, tmpFile)

  let html = await pdfToHTML(tmpFile)

  let auditorReport = await extractAuditorReport({
    page: options.page,
    html,
  })

  rmdirRecursive(dir)

  let jsonFile = options.pdfFile.replace(/\.pdf$/, '.json')
  writeFileSync(jsonFile, JSON.stringify(auditorReport, null, 2) + '\n')

  return { jsonFile }
}

async function test() {
  let browser = await chromium.launch({ headless: false })
  let page = await browser.newPage()
  await page.goto(`https://www1.hkexnews.hk/search/titlesearch.xhtml?lang=en`)
  await processPDF({
    page,
    pdfFile: 'res/2023042001897.pdf',
  })
  console.log('finished test')
  await page.close()
  await browser.close()
}
test().catch(e => console.error(e))
